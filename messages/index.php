<?php
require 'vendor/autoload.php';

$datas = json_decode(file_get_contents('php://input'), true);
//echo $datas;
//echo $datas['payload'];
//echo $datas['payload']['inputs'][0]['msg'];
//var_dump($datas['payload']['inputs']);
$url = 'http://nightcode-phobos.cleverapps.io/input/messages';
$client = new GuzzleHttp\Client([
    'headers' => [ 'Accept' => 'application/json',
    'content-type' => 'application/json',
    'X-API-Key' => '7f5a117326b3b1b5aefeef7bb325af56d26ad4ee7442e2f1dadc25fbc9d1608e' ]
]);

$messages = $datas['payload']['inputs'];

if(empty($messages))
  //http_response_code(400);
  echo "error";
foreach ($messages as $key => $message) {
  $response = $client->post($url,
    ['body' => json_encode(
        [
            'external_id' => $message['uuid'],
            'content' => $message['msg']
        ]
    )]
  );
}
